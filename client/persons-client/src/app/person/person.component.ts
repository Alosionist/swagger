import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../person';
import { Observable } from 'rxjs';
import { PersonsService } from "../persons.service";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  private id: string;
  readonly DELETE_SUCCESSFUL = 'Delete successful';

  public data: string = null;
  public person: Person;

  constructor(private personsService: PersonsService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getPerson(this.id);
  }

  async getPerson(id: string) {
    this.personsService.getPerson(id)
      .subscribe(person => this.person = person);
  }

  deletePerson(): void {
    this.personsService.deletePerson(this.person.id)
      .subscribe(() => this.data = this.DELETE_SUCCESSFUL);
  }

  get successDelete(): boolean {
    return this.data === this.DELETE_SUCCESSFUL;
  }

}
