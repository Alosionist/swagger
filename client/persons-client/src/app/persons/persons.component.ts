import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common'; 
import { Person } from "../person";
import { Observable } from 'rxjs';
import { PersonsService } from "../persons.service";


@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css']
})
export class PersonsComponent implements OnInit {

  persons$: Observable<Person[]>;

  constructor(private personsService: PersonsService,
              private location: Location) {
    this.getPersons();
   }

  ngOnInit(): void {
  }

  getPersons() {
    this.persons$ = this.personsService.getPersons();
  }
}
