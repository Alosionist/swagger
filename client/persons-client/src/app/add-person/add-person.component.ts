import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PersonsService } from "../persons.service";

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {
  readonly PERSON_ADDED = 'person added';
  public result: string;
  public personName: string;
  public personId: string;
  public personAge: number;

  constructor(private personsService: PersonsService) { }

  ngOnInit(): void {
  }

  addPerson() {
    const personToAdd: Person = {
      name: this.personName,
      id: this.personId,
      age: this.personAge
    };

    this.personsService.addPerson(personToAdd)
    .subscribe(() => this.result = this.PERSON_ADDED)
  }

  get personAdded(): boolean {
    return this.result === this.PERSON_ADDED;
  }
}
